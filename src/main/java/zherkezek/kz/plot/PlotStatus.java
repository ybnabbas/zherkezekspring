package zherkezek.kz.plot;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "plot_status")

public class PlotStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "kaz")
    private String kaz;
    @Column(name = "rus")
    private String rus;

    @OneToMany(mappedBy = "idStatus",cascade = CascadeType.ALL)
    @JsonBackReference("plot-status")
    private List<Plot> statusCollection;
}
