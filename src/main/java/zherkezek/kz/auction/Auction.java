package zherkezek.kz.auction;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "auction")
public class Auction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "title")
    @Nullable
    private String title="asd";
    @Column(name = "content")
    private String content;
    @Column(name = "date")
    private Long date=System.currentTimeMillis();
    @Column(name = "author_id")
    private int authorId;
}
