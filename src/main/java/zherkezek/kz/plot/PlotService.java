package zherkezek.kz.plot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import zherkezek.kz.cover.CoverLetter;
import zherkezek.kz.notification.Notification;
import zherkezek.kz.notification.NotificationService;
import zherkezek.kz.notification.NotificationType;
import zherkezek.kz.token.TokenService;
import zherkezek.kz.user.User;
import zherkezek.kz.user.UserRepo;
import zherkezek.kz.utils.KezekDateFormat;
import zherkezek.kz.village.Village;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class PlotService {
    @Autowired
    PlotRepo plotRepo;
    @Autowired
    NotificationService nfService;
    @Autowired
    KezekDateFormat format;
    @Autowired
    TokenService tokenDAO;

    @Autowired
    UserRepo userRepo;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    // User currentUser = userRepo.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

    public List<Plot> getPlotsMobileApp(int village, int status, int start, int finish) {
        System.out.println("Village : " + village);
        return plotRepo.getPlotsByVillage(village, status, start, finish);
    }


    public List<Plot> getAllByVillage(int villageId, int status) {
        PlotStatus plotStatus = new PlotStatus();
        plotStatus.setId(status);
        Village village = new Village();
        village.setId(villageId);

        System.out.println("Hello"+plotStatus.toString());
        return plotRepo.getAllByVillage(village, plotStatus);
    }


    public Plot get(int id) {
        return plotRepo.getOne(id);
    }


    public List<Plot> find(String key) {
        return plotRepo.find(key);
    }


    public int checkBeforeSave(Plot plot) {

        System.out.println("Count is : " + plot);
        if (plotRepo.checkBeforeSave(plot) > 0) {
            return 1;
        } else if (save(plot)) {
            return 2;
        }
        return 1;
    }

    //Өтініш қабылдау. Типі 1 болуы керек
    public boolean save(Plot plot) {

        System.out.println("Before Save result:" + plot.toString());
        Plot result = plotRepo.saveAndFlush(plot);
        System.out.println("Save result:" + result.toString());
        if (result != null) {
            Notification notification = new Notification();
            NotificationType type = new NotificationType();
            type.setId(1);
            notification.setType(type);
            notification.setIin(plot.getIin());
            nfService.save(notification);
            return true;
        } else {
            return false;
        }
    }


    //Өтініштен кейін жер коммисиясы шешім қабылдағанда осы метод шақырылады. Кезекке қабылданды немесе қабылданбады

    public boolean queuePlot(Plot plot) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepo.findByUsername(authentication.getName());
        System.out.println("Current User is : " + user.toString());
        PlotStatus plotStatus = new PlotStatus();
        plotStatus.setId(2);
        plot.setQueue(plotRepo.max(user.getVillageId().getId()) + 1);
        if (plotRepo.queuePlot(plotStatus, plot) != -1) {
            Notification notification = new Notification();
            NotificationType type = new NotificationType();
            type.setId(2);
            notification.setType(type);
            notification.setIin(plot.getIin());

            nfService.save(notification);
            return true;
        } else {
            return false;
        }
    }

    public boolean givePlot(Plot plot) {

        System.out.println("givePlot User is : " + userRepo.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).toString());
        PlotStatus plotStatus = new PlotStatus();
        plotStatus.setId(3);
        if (plotRepo.givePlot(plotStatus, plot) != -1) {
            Notification notification = new Notification();
            NotificationType type = new NotificationType();
            type.setId(3);
            notification.setType(type);
            notification.setIin(plot.getIin());

            nfService.save(notification);
            return true;
        } else {
            return false;
        }
    }

    public boolean refusePlot(Plot plot) {

        System.out.println("refusePlot User is : " + userRepo.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).toString());
        PlotStatus plotStatus = new PlotStatus();
        plotStatus.setId(4);
        if (plotRepo.refusePlot(plotStatus, plot) != -1) {
            Notification notification = new Notification();
            NotificationType type = new NotificationType();
            type.setId(4);
            notification.setType(type);
            notification.setIin(plot.getIin());

            nfService.save(notification);
            return true;
        } else {
            return false;
        }
    }

    public boolean deletePlot(Plot plot) {

        System.out.println("deletePlot User is : " + userRepo.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).toString());
        PlotStatus plotStatus = new PlotStatus();
        plotStatus.setId(5);
        if (plotRepo.deletePlot(plotStatus, plot) != -1) {
            Notification notification = new Notification();
            NotificationType type = new NotificationType();
            type.setId(5);
            notification.setType(type);
            notification.setIin(plot.getIin());

            nfService.save(notification);
            return true;
        } else {
            return false;
        }
    }
    public boolean deletePlotCoverLetter(int id) {
        if (plotRepo.deletePlotCoverLetter(id)!= -1) {
            return true;
        } else {
            return false;
        }
    }


    public boolean tempDeletePlot(Plot plot) {
        System.out.println("deletePlot User is : " + userRepo.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).toString());
        PlotStatus plotStatus = new PlotStatus();
        plotStatus.setId(6);
        if (plotRepo.deletePlot(plotStatus, plot) != -1) {
            Notification notification = new Notification();
            NotificationType type = new NotificationType();
            type.setId(6);
            notification.setType(type);
            notification.setIin(plot.getIin());

            nfService.save(notification);
            return true;
        } else {
            return false;
        }
    }

    // Модератор @CoverLetter.isEnabled=false қабылдағанда осы метод шақырыу арқылы @Plot.coverLetter =coverLetter.id орнатады
    public void setCoverLetter(CoverLetter coverLetter) {
        System.out.println("CoverLetter in PlotService : " + coverLetter);
        for (int i = 0; i < coverLetter.getPlotList().size(); i++) {
            plotRepo.setCoverLetter(coverLetter.getId(), coverLetter.getPlotList().get(i).getId());
        }

    }


    public List<Plot> getAll() {
        return plotRepo.findAll();
    }
}
