package zherkezek.kz.question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/question")
public class QuestionController {
    @Autowired
    QuestionService questionService;

    @PostMapping("/save")
    public HttpStatus save(@RequestBody Question question){
        System.out.println("Question : " +question);
      if (questionService.save(question)){
          return HttpStatus.OK;
      } else {
          return HttpStatus.CONFLICT;
      }
    }

    @GetMapping("/getListWithIin")
    public List<Question> getListWithIin(@RequestParam String iin){
        return questionService.getListWithIin(iin);
    }
    @GetMapping("/getListWithVillage")
    public List<Question> getListWithVillage(@RequestParam int village,@RequestParam int role,@RequestParam int district){
        return questionService.getListWithVillage(village,role,district);
    }

    @PostMapping("/update")
    public HttpStatus update( @RequestBody Question question){
        System.out.println("Question updated : "+question);
        if (questionService.save(question)){
            System.out.println(question);
            return HttpStatus.OK;
        } else {
            return HttpStatus.CONFLICT;
        }

    }
    @GetMapping("/getId")
    public Question getId(@RequestParam int id){
        return questionService.getId(id);
    }
}
