package zherkezek.kz.question;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "question")
//Аяқталмаған.
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int districtId;
    private int villageId;
  private String title;
    private String date;
    private String iin;
    private String phone;
    private String answer;
    private int status;
    private String village;


}
