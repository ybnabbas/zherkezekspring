package zherkezek.kz.district;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zherkezek.kz.user.UserService;

import java.util.List;

@Service
public class DistrictService {
    @Autowired
    DistrictRepo repo;
    @Autowired
    UserService userService;

    public List<District> getAll(){
        return repo.findAll();

    }
    public District save(District district){

        return repo.saveAndFlush(district);
    }

    public District getOne(Integer id){
        return repo.getOne(id);
    }

}
