package zherkezek.kz.village;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import zherkezek.kz.cover.CoverLetter;
import zherkezek.kz.district.District;
import zherkezek.kz.plot.Plot;
import zherkezek.kz.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
/*Ауылдық округ*/
@Data
@Entity
@Table(name = "village")

public class Village implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name_kaz")
    private String name;
    @Column(name = "name_rus")
    private String nameRu;
    @Column(name = "akim")
    private String akim;
    @Column(name = "phone")
    private String phone;

    @ManyToOne
    @JoinColumn(name = "district")
    private District district;

    @OneToMany(mappedBy = "idVillage",cascade = CascadeType.ALL)
    @JsonIgnore
    List<Plot> plotCollection;

    @OneToMany(mappedBy = "villageId",cascade = CascadeType.ALL)
    @JsonIgnore
    List<User> userCollection;

    @OneToMany(mappedBy = "village",cascade = CascadeType.ALL)
    @JsonBackReference
    List<CoverLetter> list;

}
