package zherkezek.kz.notification;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;

/** This class used ony {@link zherkezek.kz.plot.Plot} class changed status.
 * Moderator changed status and will called  nf to client
*
* */

@Data
@Entity
@Table(name = "notification")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id",scope = Notification.class)
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "type")
    private NotificationType type;

    @Column(name = "iin")
    private String iin;

    @Column(name = "date")
    private long date=System.currentTimeMillis();

    @Column(name = "other")
    private String other;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;
}
