package zherkezek.kz.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
    User findByUsername(String username);

    @Modifying
    @Transactional
    @Query("update User  user set user.fullName=:#{user.fullName} where user.id=:#{#user.id}")
    int updateFullname(@Param("user") User user);

    @Query("select u from User u where u.username=:username")
    User getCurrentUser(@Param("username") String username);

    @Query("select u from User u where u.username=:username and u.password=:password")
    User getUserBase64(@Param("username") String username, @Param("password") String password);

    @Query("select u from User u where u.username like %:key% or u.fullName like %:key%")
    List<User> search(@Param("key") String key);
}
