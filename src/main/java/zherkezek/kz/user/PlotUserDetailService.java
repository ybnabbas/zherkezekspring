package zherkezek.kz.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import  org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.User;

public class PlotUserDetailService implements UserDetailsService {
    @Autowired
    UserRepo userRepo;
    @Autowired
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("Username is : "+username);
        zherkezek.kz.user.User user=userRepo.findByUsername(username);

        UserBuilder builder=null;
        if (user!=null){
            builder= User.withUsername(username);
            builder.roles(user.getRole().getName());
            builder.password(bCryptPasswordEncoder().encode(user.getPassword()));
        }else  throw new UsernameNotFoundException("User not found");
return builder.build();
    }


}
