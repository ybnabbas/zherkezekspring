package zherkezek.kz.plot;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import zherkezek.kz.village.Village;

import java.util.List;

@Repository
public interface PlotRepo extends JpaRepository<Plot, Integer> {

    @Query(value = "SELECT * FROM plot WHERE id_village=:village AND id_status=:status ORDER BY queue,id LIMIT :start,:finish ", nativeQuery = true)
    List<Plot> getPlotsByVillage(@Param("village") int village, @Param("status") int status, @Param("start") int start, @Param("finish") int finish);

    @Query(value = "select plot from Plot plot where plot.idVillage=:#{#village} and plot.idStatus=:#{#status} order by queue,id")
    List<Plot> getAllByVillage(@Param("village") Village village, @Param("status") PlotStatus status);

    @Query(value = "select plot from Plot plot where plot.iin like %:key% or plot.fullname like %:key%")
    List<Plot> find(@Param("key") String key);

    @Query(value = "SELECT count(*) FROM Plot plot WHERE plot.iin=:#{#plot.iin} OR plot.fullname=:#{#plot.fullname}")
    int checkBeforeSave(@Param("plot") Plot plot);

    @Query("select max(queue) from Plot where idVillage=:id")
    int max(@Param("id") int id);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Plot plot set plot.idStatus=:status, plot.queue=:#{#plot.queue},plot.completion=:#{#plot.completion} where plot.id=:#{#plot.id}")
    int queuePlot(@Param("status") PlotStatus status, @Param("plot") Plot plot);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Plot plot set plot.idStatus=:status, plot.decision=:#{#plot.decision} where plot.id=:#{#plot.id}")
    int givePlot(@Param("status") PlotStatus status, @Param("plot") Plot plot);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Plot plot set plot.idStatus=:status, plot.decision=:#{#plot.decision} where plot.id=:#{#plot.id}")
    int refusePlot(@Param("status") PlotStatus status, @Param("plot") Plot plot);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Plot plot set plot.idStatus=:status1, plot.deleteReason=:#{#plot1.deleteReason} where plot.id=:#{#plot1.id}")
    int deletePlot(@Param("status") PlotStatus status1, @Param("plot") Plot plot1);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Plot plot set plot.idStatus=:status, plot.tempReason=:#{#plot.tempReason} where plot.id=:#{#plot.id}")
    int tempDeletePlot(@Param("status") PlotStatus status, @Param("plot") Plot plot);

    @Modifying
    @Transactional
    @Query(value = "update plot set cover_letter=:coverLetter where id=:id",nativeQuery = true)
    void setCoverLetter(@Param("coverLetter") int coverLetter, @Param("id") int id);

    @Modifying
    @Transactional
    @Query(value = "update plot set cover_letter=NULL WHERE  id=:id",nativeQuery = true)
    int deletePlotCoverLetter(@Param("id") int id);
}
