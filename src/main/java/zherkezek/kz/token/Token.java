package zherkezek.kz.token;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Data
@Table(name = "token")
@Entity
@Component
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "iin")
    private String iin;
    @Column(name = "token",columnDefinition = "TEXT")
    private String token;


}
