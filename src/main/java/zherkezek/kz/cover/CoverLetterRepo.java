package zherkezek.kz.cover;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import zherkezek.kz.district.District;
import zherkezek.kz.village.Village;

import java.util.List;

@Repository
public interface CoverLetterRepo extends JpaRepository<CoverLetter,Integer> {
    @Query("select coverLetter from CoverLetter coverLetter where coverLetter.village=:#{#village} and coverLetter.isEnabled=true")
     List findAllByVillage(@Param("village") Village village);

    @Query("select coverLetter from CoverLetter coverLetter where coverLetter.district=:#{#district} and coverLetter.isEnabled=true")
    List<CoverLetter> findAllByDistrict(@Param("district") District district);

    @Modifying
    @Transactional
    @Query("update  CoverLetter coverLetter set coverLetter.isEnabled=false where coverLetter.id=:id")
    int confirmCoverLetter(@Param("id") int id);


}
