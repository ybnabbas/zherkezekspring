package zherkezek.kz.notification;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "notification_type")
public class NotificationType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "content")
    private String content;

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL)
    @JsonBackReference("notification-type")
    List<Notification> collection;

}
