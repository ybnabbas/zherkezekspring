package zherkezek.kz.cover;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import zherkezek.kz.district.District;
import zherkezek.kz.plot.Plot;
import zherkezek.kz.plot.PlotService;
import zherkezek.kz.user.User;
import zherkezek.kz.user.UserService;
import zherkezek.kz.village.Village;
import zherkezek.kz.village.VillageService;

import java.util.List;

@RestController
@RequestMapping("/coverletter")
@CrossOrigin
@PreAuthorize("hasAnyRole('ADMIN','MODERATOR','INSPECTOR')")
public class CoverLetterController {

    @Autowired
    CoverLetterService coverLetterService;
    @Autowired
    PlotService plotService;

    @Autowired
    VillageService villageService;
    @Autowired
    UserService userService;


    @GetMapping
    @RequestMapping("all")
    public List<CoverLetter> getAll() {
        return coverLetterService.getAll();
    }

    //Осы жерде текесеріс болады. Егер модератор болса немесе инспектор болса
    @GetMapping
    @RequestMapping("/get")
    public List<CoverLetter> getAllByVillage() {
        User user = userService.getCurrentUser();
        System.out.println(user.getRole().getName());
        if (user.getRole().getName().equals("MODERATOR")) {
            System.out.println("Hello Moderator");
            System.out.println(user.getDistrictId().getId());
            return coverLetterService.getAllByDistrict(user.getDistrictId());
        } else {
            System.out.println("Hello INSPECTOR");
            return coverLetterService.getAllByVillage(user.getVillageId());
        }


    }

    @GetMapping
    public CoverLetter getOne(@Param("id") int id) {
        return coverLetterService.getOne(id);
    }

    @PostMapping
    @RequestMapping("/confirm")
    ResponseEntity confirm(@RequestBody CoverLetter coverLetter) {
        if (coverLetterService.confirmCoverLetter(coverLetter.getId()) != -1) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }


    @PostMapping
    @RequestMapping("/deleteplot")
    @PreAuthorize("hasRole('INSPECTOR')")
    ResponseEntity deletePlot(@RequestBody Plot plot) {
        System.out.println(plot);
        if (plotService.deletePlotCoverLetter(plot.getId())) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping
    @RequestMapping("/create")
    @PreAuthorize("hasRole('INSPECTOR')")
    ResponseEntity create(@RequestBody CoverLetter coverLetter) {
        System.out.println(coverLetter);
        CoverType coverType = new CoverType();

        coverType.setId(1);
        // User user= SecurityContextHolder.getContext().getAuthentication().
        // Village village=villageService.get(1);
        // coverLetter.setAkim(village.getAkim());
        User user = userService.getCurrentUser();
        Village village = new Village();
        village.setId(user.getVillageId().getId());
        District district = new District();
        district.setId(user.getDistrictId().getId());

        coverLetter.setDistrict(district);
        coverLetter.setVillage(village);
        coverLetter.setCoverType(coverType);
        System.out.println(coverLetter.toString());
        CoverLetter result = coverLetterService.saveCoverLetter(coverLetter);

        if (result != null) {
            coverLetter.setId(result.getId());
            coverLetter.setAkim(villageService.get(user.getVillageId().getId()).getAkim());
            plotService.setCoverLetter(coverLetter);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}
