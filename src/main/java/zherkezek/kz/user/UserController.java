package zherkezek.kz.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import zherkezek.kz.utils.ResponseMessage;

import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    @RequestMapping("/all")
    public List<User> getAll(){
        return  userService.getAll();
    }

    @GetMapping("/search")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> searchUser(@RequestParam("key") String key){
        return userService.search(key);
    }


    @GetMapping
    public User getCurrent() {
        return userService.getCurrentUser();
        }

    @PostMapping(value = "/authorize")
    ResponseEntity authorizeBase64(@RequestBody User user){
        System.out.println(user.toString());
        User result=userService.getUserBase64(user.getUsername(),user.getPassword());
        if (result==null){
            return new ResponseEntity(new ResponseMessage("Логин немесе құпия сөз дұрыс емес. Қайталаңыз"), HttpStatus.NOT_FOUND);
        }else {
            String forBase = result.getUsername() + ":" + result.getPassword();
            return new ResponseEntity(new ResponseMessage(Base64.getEncoder().encodeToString(forBase.getBytes())),HttpStatus.OK);
        }

    }
@PostMapping("/register")
@PreAuthorize(("hasRole('ADMIN')"))
    ResponseEntity register(@RequestBody User user) {
        System.out.println("Before Registered  user : " +user.toString());
        try {
            User result=userService.register(user);
            if (result != null) {
                String forBase = user.getUsername() + ":" + user.getPassword();
                System.out.println("After Registered  user : " + result.toString());

                return new ResponseEntity(new ResponseMessage(Base64.getEncoder().encodeToString(forBase.getBytes())), HttpStatus.OK);


            } else {
                return new ResponseEntity(new ResponseMessage("Серверде қателік"), HttpStatus.NOT_FOUND);
            }
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            System.out.println("Already exist");
            return new ResponseEntity(new ResponseMessage(user.getUsername() +" already exist"), HttpStatus.CONFLICT);
        }


    }
}


