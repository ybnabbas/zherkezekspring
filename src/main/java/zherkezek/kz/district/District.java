package zherkezek.kz.district;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import zherkezek.kz.commission.Commission;
import zherkezek.kz.cover.CoverLetter;
import zherkezek.kz.plot.Plot;
import zherkezek.kz.user.User;
import zherkezek.kz.village.Village;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/*Аудандар*/

@Data
@Entity
@Table(name = "district")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id",scope = District.class)
public class District implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "name_kz")
    String nameKz;
    @Column(name = "name_ru")
    String nameRu;

    @OneToMany(mappedBy = "district",cascade = CascadeType.ALL)
    private List<Village> villages;

    @OneToMany(mappedBy = "district",cascade = CascadeType.ALL)
    @JsonIgnore
    private List<CoverLetter> coverLetters;

    @OneToMany(mappedBy = "districtId")
    @JsonIgnore
    private List<User> users;

    @OneToMany(mappedBy = "idDistrict")
    @JsonIgnore
    private List<Plot> plotList;

    @OneToMany(mappedBy = "district")
  //  @JsonBackReference("commission-district")
    private List<Commission> commissionList;

}
