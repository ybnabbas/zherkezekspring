package zherkezek.kz.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TokenService {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    TokenRepo tokenRepo;


    public boolean save(Token token) {
        Token result = tokenRepo.isExist(token.getIin());

        if (result != null) {
            tokenRepo.update(token.getToken(), token.getIin());
            return true;
        } else {
            tokenRepo.saveAndFlush(token);
            return true;
        }


    }


    public List<Token> getAll() {
        return tokenRepo.findAll();
    }


    public List<Token> get(String iin) {
return tokenRepo.get(iin);

    }





}
