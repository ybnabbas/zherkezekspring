package zherkezek.kz.notification;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zherkezek.kz.token.Token;

import java.util.List;

@Service
public class NotificationService {
    @Autowired
    NotificationRepo notificationRepo;
    @Autowired
    zherkezek.kz.fcmserver.FCM fcm;

    @Autowired
    zherkezek.kz.token.TokenService tokenDAO;


    public void sendNotification(String token, String message, String type, int id) {
        JSONObject mainObject = new JSONObject();
        mainObject.put("title", "Жер кезек");
        mainObject.put("body", message);
        mainObject.put("sound", "default");
        mainObject.put("click_action", "kz.turkistan.AdvertActivity");

        JSONObject dataObject = new JSONObject();
        dataObject.put("type",type);
        dataObject.put("id", id);

        JSONObject full = new JSONObject();
        full.put("to", token);
        full.put("zherkezek/kz/notification", mainObject);
        full.put("data", dataObject);
        fcm.connectorFirebaseServer(full);
    }


    public List<Notification> getAll(String iin) {
        return notificationRepo.getAll(iin);
           }

    public Notification get(int id) {
      return notificationRepo.get(id);
            }


//Notification базасына тек plot статус өзгергенде ғана сақталады және жіберіледі. Ал жаңалық пен аукцион база сақталмай, бірден жіберіледі.


    public boolean save(Notification nf) {

        Notification result=notificationRepo.saveAndFlush(nf);
       if (result!=null){
           for (Token token: tokenDAO.get(nf.getIin())){
               sendNotification(token.getToken(),"Сіздің кезектегі статусыңыз өзгерді","3",result.getId());
           }
           return true;
       }else {
           return false;
       }
    }



    public boolean update(Notification nf) {
        return false;
    }


    public boolean delete(Integer nf) {
        return false;
    }


    public List<Notification> getAll() {
        return notificationRepo.findAll();
    }


}
