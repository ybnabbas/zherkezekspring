package zherkezek.kz.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepo userRepo;


    public List<User> getAll(){
        return userRepo.findAll();
    }

    public User getOne(int id){
        return userRepo.getOne(id);
    }

    public User register(User user){
        return userRepo.saveAndFlush(user);
    }

    public int updateFullname(User user ){
        return  userRepo.updateFullname(user);
    }


    public User getCurrentUser(){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        String username=authentication.getName();
        System.out.println("getCurrentUser is : " +username);
        return  userRepo.getCurrentUser(username);
    }

    public User getUserBase64(String username,String password){
        return  userRepo.getUserBase64(username,password);
    }
    public List<User> search(String key) {
        return userRepo.search(key);
    }
}
