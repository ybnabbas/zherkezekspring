package zherkezek.kz.district;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import zherkezek.kz.user.UserService;

import java.util.List;

@RestController
@RequestMapping("/district")
@CrossOrigin
public class DistrictController {
    @Autowired
    DistrictService service;
    @Autowired
    UserService userService;

   @GetMapping
   @RequestMapping("/all")
   @PreAuthorize("hasRole('ADMIN')")
    public List<District> getAll(){
       return service.getAll();
    }
    @GetMapping
    @RequestMapping("/get")
    public District getOne(@RequestParam("id") Integer id){
        return service.getOne(id);
    }
    @PostMapping
    @RequestMapping("/save")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpStatus save(@RequestBody District district){
       if (service.save(district)!=null){
           return HttpStatus.OK;
       }else {
           return HttpStatus.CONFLICT;
       }

    }

}
