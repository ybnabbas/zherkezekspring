package zherkezek.kz.plot;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import zherkezek.kz.district.District;
import zherkezek.kz.user.User;
import zherkezek.kz.user.UserService;
import zherkezek.kz.village.Village;

import java.util.List;




@RestController
@RequestMapping("/plot")
@CrossOrigin
public class PlotController {
    @Autowired
    PlotService plotService;
    @Autowired
    UserService userService;

    @RequestMapping("/find")
    @GetMapping
    public List<Plot> find(@RequestParam String key){
        return plotService.find(key);
    }
    @RequestMapping("/getList")
    @GetMapping
    public List<Plot> getPlotsMobileApp(@RequestParam int village, @RequestParam int status, @RequestParam int start, @RequestParam int finish){
        return plotService.getPlotsMobileApp(village,status,start,finish);
    }

    @RequestMapping("/getAllByVillage")
    @GetMapping
    public List<Plot> getAllByVillage(@RequestParam int status,@RequestParam int village){
        if (userService.getCurrentUser().getRole().getName().equals("MODERATOR")){
            return plotService.getAllByVillage(village,status);
        }else {
            return plotService.getAllByVillage(userService.getCurrentUser().getVillageId().getId(),status);
        }


    }
    @RequestMapping("/all")
    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public List<Plot> getAll(){
        return plotService.getAll();


    }


    @RequestMapping("/get")
    @GetMapping
    public Plot get(@RequestParam int id){
        return plotService.get(id);
    }

    @RequestMapping("/check")
    @PostMapping
    @PreAuthorize("hasRole('INSPECTOR')")
    public ResponseEntity check(@RequestBody Plot plot){
        System.out.println(plot);
        User currentUser=userService.getCurrentUser();

        District district = new District();
        district.setId(currentUser.getDistrictId().getId());
        plot.setIdDistrict(district);

        Village village = new Village();
        village.setId(currentUser.getVillageId().getId());
        plot.setIdVillage(village);

        PlotStatus plotStatus = new PlotStatus();
        plotStatus.setId(1);
        plot.setIdStatus(plotStatus);

        int result= plotService.checkBeforeSave(plot);
        System.out.println("Result after check in Controller : "+result);
        switch (result){
            case 1:
                return ResponseEntity.badRequest().build();

            case 2:

                return ResponseEntity.ok().build();
                default:
                    return ResponseEntity.notFound().build();
        }


    }
    //Егер Инспектор базада бола тұра қабылдаса осы шақырылады

    @RequestMapping("/save")
    @PostMapping
    @PreAuthorize("hasRole('INSPECTOR')")
    public HttpStatus save(@RequestBody Plot plot){
        User currentUser=userService.getCurrentUser();
        District district = new District();
        district.setId(currentUser.getDistrictId().getId());
        plot.setIdDistrict(district);

        Village village = new Village();
        village.setId(currentUser.getVillageId().getId());
        plot.setIdVillage(village);

        PlotStatus plotStatus = new PlotStatus();
        plotStatus.setId(1);
        plot.setIdStatus(plotStatus);

        System.out.println(plot);
        if (plotService.save(plot)){
            return HttpStatus.OK;
        }else {
            return HttpStatus.CONFLICT;
        }

    }

    @RequestMapping("/update")
    @PostMapping
    public HttpStatus update(@RequestBody String json){
        System.out.println(json);
        JSONObject jsonObject=new JSONObject(json);
        int status=jsonObject.getInt("status");

        System.out.println(jsonObject.getJSONArray("users"));
       JSONArray users=jsonObject.getJSONArray("users");

        System.out.println("Users is " +users);
        for ( int i=0;i<users.length();i++){
            Plot plot = new Plot();
            PlotStatus plotStatus =new PlotStatus();
            plotStatus.setId(status);
            plot.setIdStatus(plotStatus);

            System.out.println("User get(i) : " +users.getJSONObject(i).getInt("id"));
            plot.setId(users.getJSONObject(i).getInt("id"));
            String info=jsonObject.getString("info");

            switch (status){
                case 2:
                    plotService.queuePlot(plot);
            }


        }
        return HttpStatus.OK;


    }

}
