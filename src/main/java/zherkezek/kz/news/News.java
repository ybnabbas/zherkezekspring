package zherkezek.kz.news;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "news")
public class News implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "title",columnDefinition="TEXT")
    String title;

    @Column(name = "content",columnDefinition="TEXT")
    String content;
    @Column(name = "date")
    Long date=System.currentTimeMillis();
}
