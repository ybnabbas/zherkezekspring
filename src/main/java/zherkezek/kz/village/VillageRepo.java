package zherkezek.kz.village;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zherkezek.kz.district.District;

import java.util.List;
@Repository
public interface VillageRepo extends JpaRepository<Village,Integer> {

@Query("select village from Village  village where village.district=:#{#district}")
     List<Village> getListByDistrict(@Param("district") District district);
@Query("select village from Village village where village.name like %:key% or village.nameRu like %:key%")
     List<Village> find(String key);
}
