package zherkezek.kz.cover;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Table
@Entity

public class CoverType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "coverType",cascade = CascadeType.ALL)
    @JsonBackReference("coverletter-type")
    List<CoverLetter> list;

}
