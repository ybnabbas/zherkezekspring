package zherkezek.kz.commission;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zherkezek.kz.district.District;

import java.util.List;

@Repository
public interface CommissionRepo extends JpaRepository<Commission,Integer> {
    @Query("select commission  from Commission commission where commission.district=:#{#district}")
    List<Commission> getAllByDistrict(@Param("district") District district);
}
