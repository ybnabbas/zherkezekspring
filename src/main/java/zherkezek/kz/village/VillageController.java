package zherkezek.kz.village;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import zherkezek.kz.user.UserService;

import java.util.List;

@RestController
@RequestMapping("/village")
@CrossOrigin
public class VillageController {
    @Autowired
    VillageService villageService;
    @Autowired
    UserService userService;
    @GetMapping
    public Village getVillage(@RequestParam int id){
        return villageService.get(id);
    }

    @GetMapping
    @RequestMapping("/list")
    public List<Village> getListVillageByDistrict(){
        return villageService.getListByDistrict(userService.getCurrentUser().getDistrictId());
    }

    @GetMapping
    @RequestMapping("/find")
    public List<Village> find(@RequestParam String  key){
        return villageService.find(key);
    }

    @GetMapping
    @RequestMapping("/all")
    public List<Village> getAllVillage(){
        return villageService.getAll();
    }

    @PostMapping
    @RequestMapping("/save")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity add(@RequestBody Village village){
        if (villageService.add(village)!=null){
            return ResponseEntity.ok().body("Қосылды");
        }else {
            return ResponseEntity.badRequest().body("Қосылмады");
        }
    }


}
