package zherkezek.kz.commission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zherkezek.kz.district.District;

import java.util.List;

@Service
public class CommissionService {

    @Autowired
    CommissionRepo repo;

    public List<Commission> getAll(){
        return repo.findAll();
    }

    public Commission getOne(int id){
        return repo.getOne(id);
    }

    public List<Commission>getAllByDistrict(District district){
    return repo.getAllByDistrict(district);
    }


    public Commission save(Commission commission) {
        return repo.save(commission);
    }

    public void delete(Commission commission) throws IllegalAccessException {
       repo.delete(commission);
    }
}
