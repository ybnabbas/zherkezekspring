package zherkezek.kz.utils;

import org.springframework.stereotype.Component;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
@Component

public class KezekDateFormat {

    public SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy ", formatSymbols);
    private static DateFormatSymbols formatSymbols = new DateFormatSymbols() {
        @Override
        public String[] getMonths() {
            return new String[]{"Қаңтар", "Ақпан", "Наурыз", "Сәуір", "Мамыр", "Маусым", "Шілде", "Тамыз", "Қыркүйек", "Қазан", "Қараша", "Желтоқсан"};
        }
    };
}
