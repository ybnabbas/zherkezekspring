package zherkezek.kz.auction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RequestMapping("/auction")
@RestController
public class AuctionController {
    @Autowired
    AuctionService service;

    @GetMapping
    @RequestMapping("/all")
    public List<Auction> getAll(){
        return service.getAll();
    }

    @PostMapping
    @RequestMapping("/save")
    public HttpStatus save(@RequestBody Auction auction){
        if (service.save(auction)){
           return  HttpStatus.OK;
        }else {
           return HttpStatus.CONFLICT;
        }

    }
    @GetMapping
    @RequestMapping("/get")

    public Auction getOne(@RequestParam("id") int id){
        return service.get(id);
    }
}
