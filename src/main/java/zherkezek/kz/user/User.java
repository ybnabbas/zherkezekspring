package zherkezek.kz.user;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import zherkezek.kz.district.District;
import zherkezek.kz.village.Village;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "user")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id",scope = User.class)
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column(name = "fullname")
    String fullName;

    @Column(name = "username", unique = true)
    String username;

    @Column(name = "password")
    String password;
    @Column(name = "enabled")
    boolean enabled = true;

    @ManyToOne
    @JoinColumn(name = "role")
    Role role;

    @ManyToOne
    @JoinColumn(name = "villageId")
    Village villageId;

    @ManyToOne
    @JoinColumn(name = "districtId")
    District districtId;


}
