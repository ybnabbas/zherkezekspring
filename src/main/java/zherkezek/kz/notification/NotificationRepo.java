package zherkezek.kz.notification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface NotificationRepo extends JpaRepository<Notification,Integer> {

    //void sendNotification(String token, String message, String type, int id);
    @Query("select nf from Notification nf where nf.iin like %:iin%")
    List<Notification> getAll(@Param("iin") String iin);
    @Query("select nf from Notification nf where nf.id=:id")
    Notification get(@Param("id") int id);
}
