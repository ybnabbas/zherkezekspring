package zherkezek.kz.commission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import zherkezek.kz.district.District;
import zherkezek.kz.user.UserService;

import java.util.List;

@RestController
@RequestMapping("/commission")
@CrossOrigin
public class CommissionController {
@Autowired
CommissionService service;
@Autowired
    UserService userService;
    @GetMapping
    @RequestMapping("/all")
    public List<Commission> getAll(){
        return service.getAll();
    }

    @GetMapping
    @RequestMapping("/allbydistrict")
    @PreAuthorize("hasRole('ADMIN')")
    public List<Commission> getAllByDistrict(){
        District district = new District();
        district.setId(userService.getCurrentUser().getDistrictId().getId());
        return service.getAllByDistrict(district);
    }
    @GetMapping
    public Commission getOne(@RequestParam("id") int id){
        return service.getOne(id);
    }

@RequestMapping("/save")
@PostMapping
@PreAuthorize("hasAnyRole('MODERATOR','ADMIN')")
    public ResponseEntity save(@RequestBody Commission commission){
        District district = new District();
        district.setId(userService.getCurrentUser().getDistrictId().getId());
        commission.setDistrict(district);
        if (service.save(commission)!=null){

            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.badRequest().build();
        }
    }
    @RequestMapping("/delete")
    @PostMapping
    @PreAuthorize("hasAnyRole('MODERATOR','ADMIN')")
    public ResponseEntity delete(@RequestBody Commission commission){
        try {
            service.delete(commission);
            return ResponseEntity.ok().build();
        }catch (IllegalAccessException e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }

    }
}
