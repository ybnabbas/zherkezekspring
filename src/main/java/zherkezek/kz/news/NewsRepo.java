package zherkezek.kz.news;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface NewsRepo extends JpaRepository<News,Integer> {
    @Query("select news from News news where news.content like %:key% or  news.title like %:key%")
    public List<News> find(@Param("key") String key);

    @Query(value = "SELECT * FROM news ORDER BY id DESC LIMIT :starta , :finish",nativeQuery = true)
    public List<News> getList(@Param("start") int starta,@Param("finish") int finish);
}
