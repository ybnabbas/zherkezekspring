package zherkezek.kz.village;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zherkezek.kz.district.District;

import java.util.List;


@Service
public class VillageService {
@Autowired
    VillageRepo villageRepo;


    public Village get(int id) {
       return villageRepo.getOne(id);

    }


    public List<Village> find(String key) {
        return villageRepo.find(key);
    }


    public boolean save(Village village) {
       if (villageRepo.saveAndFlush(village)!=null){
           return true;
       }else {
           return false;
       }
    }


    public boolean update(Village plot) {
        return false;
    }


    public boolean delete(Integer plot) {
        return false;
    }


    public List<Village> getAll() {
        return villageRepo.findAll();
    }


    public List<Village> getListByDistrict(District district) {
        return villageRepo.getListByDistrict(district);


    }


    public Village add(Village village) {
        return villageRepo.saveAndFlush(village);
    }
}
