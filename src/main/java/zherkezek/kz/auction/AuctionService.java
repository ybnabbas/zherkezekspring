package zherkezek.kz.auction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zherkezek.kz.notification.NotificationService;
import zherkezek.kz.token.Token;


import java.util.List;

@Service
public class AuctionService {

    @Autowired
    AuctionRepo auctionRepo;

    @Autowired(required = true)
    Token tokenDAO;
    @Autowired
    NotificationService nfDao;


    public boolean save(Auction auction) {

        Auction result = auctionRepo.saveAndFlush(auction);
        if (result != null) {
            nfDao.sendNotification("/topics/auction", " Аукцион : " + auction.getTitle(), "2", result.getId());
            return true;
        } else {
            return false;
        }
    }


    public List<Auction> getAll() {
        return auctionRepo.findAll();
        }

    public Auction get(int id) {
        return auctionRepo.getOne(id);
        }
}
