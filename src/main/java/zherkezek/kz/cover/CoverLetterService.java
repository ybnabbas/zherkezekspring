package zherkezek.kz.cover;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zherkezek.kz.district.District;
import zherkezek.kz.village.Village;

import java.util.List;

@Service
public class CoverLetterService {

    @Autowired
    CoverLetterRepo repo;




    public List<CoverLetter> getAll(){
        return  repo.findAll();
    }
//Округ мамандары осыдан алады
    public List<CoverLetter> getAllByVillage(Village village){
        return repo.findAllByVillage(village);
    }
// Жер коммиссия маманы осыдан алады
    public List<CoverLetter> getAllByDistrict(District district){
        return repo.findAllByDistrict(district);
    }

    public CoverLetter getOne(int id){
        return  repo.getOne(id);
    }

//Модератор қабылдағанда статусы false болады. Тек Модератор шақырады
     public int confirmCoverLetter(int id){
        return  repo.confirmCoverLetter(id);
    }
//Округтің жер маманы осыны шақыру арқылы жаңа Ілеспе хат жасайды.
    public CoverLetter saveCoverLetter(CoverLetter coverLetter){
        return  repo.saveAndFlush(coverLetter);
    }

}
