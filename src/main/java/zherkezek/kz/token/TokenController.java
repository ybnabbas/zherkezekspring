package zherkezek.kz.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/token")

public class TokenController {
    @Autowired
    TokenService dao;

    @PostMapping("/add")
    public ResponseEntity addToken(@RequestBody Token token) {
        if (dao.save(token)){
            return ResponseEntity.ok().build();
        }
        else {
            return ResponseEntity.badRequest().build();
        }

    }
}
