package zherkezek.kz.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("notification")
@RestController
@CrossOrigin
public class NotificationController {
    @Autowired
    NotificationService notificationDAO;

    @GetMapping("/all")
    public List<Notification> getAll(@RequestParam String iin){
        return notificationDAO.getAll(iin);

    }
    @GetMapping("/get")
    public Notification get(@RequestParam int id){
        return notificationDAO.get(id);

    }

}
