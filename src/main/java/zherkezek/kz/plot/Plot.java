 package zherkezek.kz.plot;


 import com.fasterxml.jackson.annotation.JsonIdentityInfo;
 import com.fasterxml.jackson.annotation.JsonIgnore;
 import com.fasterxml.jackson.annotation.ObjectIdGenerators;
 import lombok.Data;
import org.springframework.lang.Nullable;
import zherkezek.kz.cover.CoverLetter;
import zherkezek.kz.district.District;
import zherkezek.kz.village.Village;

import javax.persistence.*;
import java.io.Serializable;

 @Data
@Entity
@Table(name = "plot")
 @JsonIdentityInfo(generator = ObjectIdGenerators.None.class,property = "id",scope = Plot.class)
 public class Plot implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "queue")
    private int queue=0;

    @Nullable
    @Column(name = "iin")
    private String iin;

    @Column(name = "fullname")
    private String fullname;

    @ManyToOne
    @JoinColumn(name = "idVillage")
    private Village idVillage;

    @ManyToOne
    @JoinColumn(name = "idDistrict")
    @JsonIgnore
    private District idDistrict;

    @ManyToOne
    @JoinColumn(name = "idStatus")
    private PlotStatus idStatus;

    @ManyToOne
    @JoinColumn(name = "cover_letter")
    @Nullable
    CoverLetter coverLetter;

    @Column(name = "apply_date")
    private String applyDate;
    @Nullable
    @Column(name = "completion")
    private String completion;
    @Nullable
    @Column(name = "decision")
    private String decision;
    @Nullable
    @Column(name = "tempReason")
    private String tempReason;
    @Nullable
    @Column(name = "deleteReason")
    private String deleteReason;
    @Nullable
    @Column(name = "phone")
    private String phone;

     @Nullable
     @Column
     private Long systemTime=System.currentTimeMillis();
}
