package zherkezek.kz.commission;

import lombok.Data;
import org.springframework.lang.Nullable;
import zherkezek.kz.district.District;

import javax.persistence.*;
import java.io.Serializable;

/*
* Бұл класс Аудандағы коммиссялар тізімін жасау үшін арналған. Көп жағдайда Жер қатынастар бөлімінің маманы қолданады.
* Қорытынды шығаруға қажет болғаны үшін жасалынды
* */


@Data
@Entity
@Table(name = "commission")

public class Commission implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "fullname")
    private String fullname;
    @Column(name = "position")
    private String position;

    @Column(name = "added_time")
    @Nullable
    private Long addedTime=System.currentTimeMillis();

    @Column(name = "is_active",columnDefinition = "boolean default true")
    private boolean isActive=true;

    @ManyToOne
    @JoinColumn(name = "district")
    District district;

}
