package zherkezek.kz.news;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/news")
public class NewsController {
    @Autowired
    NewsService newsDAO;

    @GetMapping
    @RequestMapping("/list")
    List<News> newsList(@RequestParam int start, @RequestParam int finish){
        return newsDAO.getList(start,finish);
    }

    @GetMapping
    @RequestMapping("/get")
    News news(@RequestParam int id){
        return newsDAO.get(id);
    }

    @PostMapping
    @RequestMapping("/save")
    HttpStatus news(@RequestBody News news){
        if (newsDAO.save(news))
        {
            return HttpStatus.OK;
        }else {
            return HttpStatus.CONFLICT;
        }
    }
}
