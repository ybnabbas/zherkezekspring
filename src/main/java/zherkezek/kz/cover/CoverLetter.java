package zherkezek.kz.cover;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import zherkezek.kz.district.District;
import zherkezek.kz.plot.Plot;
import zherkezek.kz.village.Village;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/*
* Ілеспе хат. Бірінші округтың жер мамандары Plot тандап, осыны жасайды. Кейін тізімде жасалған барлық ілеспе хаттардан көре алады
*
*
*
* */


@Data
@Table(name = "cover")
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class CoverLetter implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "type")
    private CoverType coverType;

    @Column(name = "akim")
    private String akim;

    @ManyToOne
    @JoinColumn(name ="village")
    Village village;

    @ManyToOne
    @JoinColumn(name ="district")
    District district;

    @Column(name = "isEnabled",columnDefinition = "boolean default true")
    private boolean isEnabled=true;

    @Column(name = "date")
    private Long date=System.currentTimeMillis();

    @OneToMany(mappedBy = "coverLetter")
    List<Plot> plotList;


}
