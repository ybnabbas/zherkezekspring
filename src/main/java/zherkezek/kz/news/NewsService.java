package zherkezek.kz.news;

import zherkezek.kz.notification.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsService {
    @Autowired
    NewsRepo newsRepo;
    @Autowired
    NotificationService nfDao;


    public List find(String key) {
        return newsRepo.find(key);
    }


    public List<News> getList(int start, int finish) {
        //language=sql

        return newsRepo.getList(start, finish);

    }


    public News get(int id) {
        return (News) newsRepo.getOne(id);
    }


    public boolean save(News news) {
        News result = newsRepo.saveAndFlush(news);
        if (result != null) {
            nfDao.sendNotification("/topics/news", " Жаңалық : " + news.getTitle(), "1", result.getId());
            return true;
        } else {
            return false;
        }


    }


    public List<News> getAll() {
        return newsRepo.findAll();
    }

}
