package zherkezek.kz.fcmserver;



import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.*;

/**
 * Created by Ilyas on 20.09.2017.
 */
@Component
public class FCM {

    final static private String FCM_URL = "https://fcm.googleapis.com/fcm/send";
    final static private String SERVER_KEY = "AAAAnY1mZQ0:APA91bFRopAtoJTb5OOUxsqAhSblEY0AQXDVy2u4jfgPRv8FHl0wMmwXirqhM-2DmGLRr8WcwU1tnFZNvevkZegpbu6iQUeCAOLP9JxRfvzUDafSeI-3_1nBiJYrd28XcdBOfgDTUw41";

    public void connectorFirebaseServer(JSONObject full) {
        //https://firebase.google.com/docs/cloud-messaging/http-server-ref
        try {
            URL url = new URL(FCM_URL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Authorization", "key=" + SERVER_KEY);
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8 ");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(full.toString());
            writer.flush();
            int status = 0;
            if (connection != null) {
                status = connection.getResponseCode();
            }
            System.out.println("status is : " + status);
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            System.out.println(full.toString());
            System.out.println("Android Notification Response : " + reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }





}
