package zherkezek.kz.token;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
public interface TokenRepo extends JpaRepository<Token,Integer> {

    @Query("select token from Token token where token.iin=:iin")
    List<Token> get(@Param("iin") String iin);

    @Query("select  token from Token token where  token.iin=:iin")
    Token isExist(@Param("iin") String iin);

    @Transactional
    @Modifying
    @Query("update Token token set token.token=:token where token.iin=:iin")
    int update(@Param("token")String token, @Param("iin") String iin);


}
